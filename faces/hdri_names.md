# HDRI Names

Valid HDRI names:

```json
[
  "apartment",
  "arches_pinetree",
  "barcelona_rooftops",
  "basketball_court",
  "brooklyn_bridge_planks",
  "bryant_park",
  "chelsea_stairs",
  "chiricahua_narrowpath",
  "chiricahua_plaza",
  "circus_backstage",
  "city_night_lights",
  "cloudy_sky",
  "containers",
  "desert_highway",
  "ditch_river",
  "etniespark_center",
  "factory_catwalk",
  "field_afternoon",
  "footprint_court",
  "frozen_waterfall",
  "grandcanyon_yumapoint",
  "green_path",
  "hamarikyu_bridge",
  "harbor",
  "helipad_afternoon",
  "helipad_goldenhour",
  "hollywood_sign",
  "ice_lake",
  "malibu_overlook",
  "mono_lake_1",
  "mono_lake_2",
  "monvalley_dirtroad",
  "monvalley_lookout",
  "newport_loft",
  "old_industrial_hall",
  "papermill_ruins_1",
  "papermill_ruins_2",
  "parking_lot",
  "playa_sunrise",
  "popcorn_lobby",
  "power_plant",
  "queenmary_chimney",
  "ridgecrest_road",
  "river_road",
  "serpentine_valley",
  "shiodome_stairs",
  "sierra_madre",
  "snow_machine",
  "stadium_center",
  "stone_wall",
  "subway_lights",
  "summi_pool",
  "theatre_center",
  "theatre_seating",
  "tokyo_bigsight",
  "topanga_forest",
  "tropical_beach",
  "tropical_ruins",
  "tunnel",
  "ueno_shrine",
  "upperfloor",
  "walk_of_fame",
  "waterfall",
  "winter_forest",
  "wooden_door",
  "yaris_interior",
  "zion_sunsetpeek"
]
```
