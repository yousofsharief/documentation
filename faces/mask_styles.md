# Mask Styles

Valid mask styles:

```json
[
  "unisex_debrief_me",
	"unisex_debrief_me_off_nose",
	"unisex_dukal",
	"unisex_dukal_off_nose",
	"unisex_generic_1",
	"unisex_generic_1_off_nose",
	"unisex_hats_n_tales_cotton",
	"unisex_hats_n_tales_cotton_off_nose",
	"unisex_uline_deluxe_surgical",
	"unisex_uline_deluxe_surgical_off_nose",
	"unisex_airpop_shell",
	"unisex_airpop_shell_off_nose"
]
```
