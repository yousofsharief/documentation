# Glasses Styles & Colors

Valid glasses styles:

```json
[
  "female_butterfly_sunglasses",
  "female_cat_eye_frame_in_tortoise",
  "female_cat_eye_reading",
  "female_oval_plastic",
  "female_square_sunglasses",
  "male_metal_aviator",
  "male_mirror_aviator_sunglasses",
  "male_plastic_aviator",
  "male_round_frame_in_clear",
  "male_single_line_aviator",
  "male_square_frame_in_clear",
  "male_wire_round_frame"
]
```

Valid glasses colors:

```json
[
  "default",
	"clear",
	"blue",
	"red",
	"grey",
	"grey_trans"
]
```
